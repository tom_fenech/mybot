import os
import imp
import sys
from handlers.handlerBase import HandlerBase

ssl = False
serverPassword = ''
testing = False

chatLogDirectory = 'logs/chat'
constantsUpdateFreq = 5
maxAuthTries = 1
averageErrorPer = 120
elevateDuration = 2
spamCount = 6
spamFrequency = 11
spamCleanSlateDays = 7

enableRandomTypos = True

autoAssignLevels = {}
protectedNames = ['chanserv', 'nickserv']
floodPreventionChannels = []

def loadUserConfig(path):
    try:
        __name__ = 'config'
        moduleName = os.path.splitext(os.path.basename(path))[0]
        if os.path.exists(path):
            module = imp.load_source(moduleName, path)
            sys.modules[__name__].__dict__.update(module.__dict__)
        else:
            print 'user configuration file', path, 'not found'
    except Exception, e:
        print 'failed to merge configs'
        print e

def enabled(plugin):
    return plugin in handlers.keys()

def getAutoLevel(user, destination):
    destinationAutoLevels = autoAssignLevels[destination]

    userLevels = HandlerBase.getLevelsForUser(user)
    for level in userLevels:
        if level in destinationAutoLevels.keys():
            return destinationAutoLevels[level]
    return None
