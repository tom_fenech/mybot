from migration.migration import Migration

class AddEventsTable(Migration):
    def __init__(self):
        Migration.__init__(self, '201401051231')

    def getSql(self):
        return 'create table event ([id] integer primary key autoincrement, [name] text, [description] text, [startDate] timestamp, [endDate] timestamp, [listName] text, [steamEventId] text)'
