import config
import handler
from handlerBase import HandlerBase, Command

class ReloadHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!reload', self.handleReload))
		HandlerBase.__init__(self, levels, Command('!unload', self.handleUnload))
		self.protectedCoreModules = ['handler', 'irc', 'ircbot']

	def handleReload(self, message):
		if message.numArgs == 1:
			if handler.isCoreModule(message.args[0]) and message.args[0] not in self.protectedCoreModules:
				result = handler.reloadCoreModule(message.args[0])
				if result == None:
					self.sendMsg(message.sender, 'core module \'' + message.args[0] + '\' reloaded')
				else:
					self.sendMsg(message.sender, str(result))
			elif message.args[0] in config.handlers:
				levels = config.handlers[message.args[0]]
				handler.unloadPlugin(message.args[0])
				result = handler.loadPlugin(message.args[0], levels)
				if result == None:
					self.sendMsg(message.sender, 'plugin \'' + message.args[0] + '\' reloaded')
				else:
					self.sendMsg(message.sender, str(result))
			else:
				self.sendMsg(message.sender, 'could not find module ' + message.args[0])
		else:
			self.sendMsg(message.sender, 'usage: !reload <module>')

	def handleUnload(self, message):
		if message.numArgs == 1:
			if handler.unloadPlugin(message.args[0]):
				self.sendMsg(message.sender, 'plugin \'' + message.args[0] + '\' unloaded')
			else:
				self.sendMsg(message.sender, 'plugin \'' + message.args[0] + '\' not found')
		else:
			self.sendMsg(message.sender, 'usage: !unload <module>')

def __init__(levels):
	ReloadHandler(levels)
