import constants
from handlerBase import HandlerBase, Command

class UpdateConstantsHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!updconst', self.handle))

	def handle(self, message):
		if message.numArgs >= 2:
			constants.updateConstant(message.args[0], ' '.join(message.args[1:]))
			self.sendMsg(message.sender, 'Constant updated')
		else:
			self.sendMsg(message.sender, 'usage: !updconst <name> <value>')
			self.sendMsg(message.sender, 'Available constants: ' + constants.getConstants())

def __init__(levels):
	UpdateConstantsHandler(levels)
