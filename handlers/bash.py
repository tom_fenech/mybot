import constants
from handlerBase import HandlerBase, Command

class BashHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!bash', self.handle, 'A quick link to the thread on the forums where all the hilarious moments in the channel are captured forever', '!bash'))

	def handle(self, message):
		self.sendMsg(message.sender, constants.getConstant('bashUrl'))

def __init__(levels):
	BashHandler(levels)
