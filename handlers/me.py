from handlerBase import HandlerBase, Command

class MeHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!me', self.handle))

	def handle(self, message):
		if message.numArgs >= 2:
			self.sendMsg(message.args[0], '\x01ACTION ' + ' '.join(message.args[1:]) + '\x01')
		else:
			self.sendMsg(message.sender, 'usage: !msg <destination> <message>')

def __init__(levels):
	MeHandler(levels)
