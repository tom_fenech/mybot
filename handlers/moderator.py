import config
import constants
import database
import ircCommands
from handlerBase import HandlerBase, Command
import handler

class ModeratorHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!modadd', self.addModerator))
		HandlerBase.__init__(self, levels, Command('!moddel', self.delModerator))

	def addModerator(self, message):
		if self.validate(message, 'add'):
			self.getHandler('usergroups').addUserToGroup(message.sender, message.args[0], 'moderator')
			self.setUserLevel(message.args[0], True)

	def delModerator(self, message):
		if self.validate(message, 'del'):
			self.getHandler('usergroups').removeUserFromGroup(message.sender, message.args[0], 'moderator')
			self.setUserLevel(message.args[0], False)

	def setUserLevel(self, user, give):
		if 'moderator' in config.autoAssignLevels:
			mode = config.autoAssignLevels['moderator']
			ircCommands.sendCommand(user, config.channel, mode, give)
	
	def validate(self, message, operation):
		if message.numArgs == 0:
			self.sendMsg(message.sender, 'usage: !mod%s <user>' % operation)
			return False
		return True

def __init__(levels):
	ModeratorHandler(levels)
