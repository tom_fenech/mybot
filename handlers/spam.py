import userTracker
from handlerBase import HandlerBase, Command
import handler

class SpamHandler(HandlerBase):

	def __init__(self, levels):
		self.usage = '!spam <user>'
		HandlerBase.__init__(self, levels, Command('!spam', self.handle, 'Raises a user\'s spam warning level by one. This will behave exactly as if the bot had done it by itself - you will not be publicly revealed if you use this command privately. The user will be warned via private message, and they may have a temporary ban applied', self.usage))
		
	def handle(self, message):
		if message.numArgs != 1:
			self.sendMsg(message.sender, 'usage: ' + self.usage)
			return

		if userTracker.isOnline(message.args[0]):
			userTracker.elevateSpamLevel(message.args[0])
			self.sendMsg(message.sender, 'User spam level has been raised and user has been informed, just as if the bot had triggered this automatically')
		else:
			self.sendMsg(message.sender, message.args[0] + ' does not appear to be online at the moment. Nothing to do')

def __init__(levels):
	SpamHandler(levels)
