import config
import constants
import database
from handlerBase import HandlerBase, Command
import handler

class HelpHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!help', self.handle, 'You need help using help? Oh brother...', '!help [help topic | command name]'))
	#	self.topics = {
	#		'registration': ['Registering your nickname prevents other people from pretending to be you. Visit http://www.cogsbristol.co.uk/index.php/guides/irc-guide/ for help about registering your name'],
	#		'nickname': ['If you\'re new here, you might be interested to know you can change your name by typing /nick <name> (replace <name> with what you want to be called) in the chat window'],
	#		'about': ['WildFire is my creator/maintainer. If you have any questions, comments, or suggestions send him a private message on IRC'],
	#		'commands': [u'Typing \u0002!help\u0002 with no arguments will show you the list of available commands.', u'Don\'t be afraid to play with the commands. If a command requires arguments you will be told what they are. An argument in square brackets is [optional]. Required arguments are in <angled> brackets. e.g. \u0002!remind [date] <time> <reminder>']
	#	}
	#	if config.enabled('lists'):
	#		self.topics['lists'] = [constants.getConstant('listHelp'), constants.getConstant('gamingListHelp')]
	#	topicKeys = self.topics.keys()
	#	topicKeys.sort()
	#	self.topicKeys = ', '.join(topicKeys)

	def handle(self, message):
		help = []

		if message.sender.startswith('COGSGuest'):
			help.append('Hello, ' + message.sender + u'. If you want to change your name so you aren\'t called ' + u'\u0002' + message.sender + u'\u0002 anymore, type "\u0002/nick yournewname\u0002" (without quotes obviously)')

		if message.numArgs == 0:
			help.append(u'For the list of commands available to you, type \u0002!help commands\u0002')
			help.append(u'For specific help with a command, type \u0002!help\u0002 followed by the name of one of the commands, e.g. \u0002!help lan\u0002')
			if 'lists' in config.handlers:
				help.append(u'To see the current events/games lists, type \u0002!help lists\u0002')
			if config.enabled('mod'):
				help.append(u'If you need to attract the attention of a moderator, you can type \u0002!mod [optional message]\u0002')

		elif message.numArgs == 1:
			if message.args[0] == 'commands':
				helpText = u'Standard commands: \u0002'
				commands = handler.getCommandsForLevel('all')
				commands.sort()
				helpText += ' '.join(commands)
				help.append(helpText)
			
				authedUser = self.isAuthedUser(message.sender)
				if authedUser:
					levels = self.getLevelsForUser(message.sender)
					for level in levels:
						levelHelp = level + u' commands: \u0002'
						commands = handler.getCommandsForLevel(level)
						commands.sort()
						levelHelp += ' '.join(commands)
						help.append(levelHelp)

			elif message.args[0] == 'lists':
				if 'lists' in config.handlers:
					help.append(u'To learn how to use lists, type \u0002!help listusage\u0002')
					listHelp = u'Available lists: \u0002'
					gameListHelp = u'Available game lists: \u0002'
					lists = self.getHandler('lists').getLists()
					gameLists = self.getHandler('lists').getGameLists()
					self.listSubtract(lists, gameLists)
					listHelp += ' '.join(lists)
					gameListHelp += ' '.join(gameLists)

					if len(lists) > 0:
						help.append(listHelp)
					if len(gameLists) > 0:
						help.append(gameListHelp)

			elif message.args[0] == 'listusage':
				if 'lists' in config.handlers:
					help.append(constants.getConstant('listHelp'))
					help.append(constants.getConstant('gamingListHelp'))

			else:
				if not message.args[0].startswith('!'):
					userText = '!' + message.args[0]
				else:
					userText = message.args[0]
				userLevels = self.getLevelsForUser(message.sender)
				level = handler.getLevelUserCanRunCommandAt(userText, userLevels)
				if level != None:
					if level == 'all' or self.isAuthedUser(message.sender):
						info = handler.getInfoForCommandAtLevel(userText, level)
						self.sendMsg(message.sender, info.help)
						self.sendMsg(message.sender, 'usage: ' + info.usage)

				else:
					self.sendMsg(message.sender, 'I\'m not sure what you want me to help you with. If you are stuck, pass any queries on to WildFire, who will be happy to help')

		else:
			help.append('usage: !help [topic]')

		self.sendMultilineMsg(message.sender, help)

	def listSubtract(self, master, child):
		for item in child:
			if item in master:
				master.remove(item)

def __init__(levels):
	HelpHandler(levels)
