import config
import constants
import timeUtils
from handlerBase import HandlerBase, Command
import handler
import database
import re
from datetime import datetime

class AwayHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!away', self.handleAway, u'This sets you as away. Whilst you are away, you will not be sent notifications when people want to play games. One might use this if you didn\'t want to be disturbed for a period of time. To set yourself as back, you can either use \u0002!back\u0002, or you can supply an optional duration on the end - after which time you will be automatically set as back', '!away [time]'))
		HandlerBase.__init__(self, levels, Command('!back', self.handleBack, u'Used in conjunction with \u0002!away\u0002. Restores your status to back, so that you will receive game notifications again' ,'!back'))

		self.persistentType = 'data'
		self.away = {}
		self.persistentData = database.retrievePersistentStore(self.persistentType)
		for item in self.persistentData:
			if item[1] == 'None':
				self.away[item[0]] = None
			else:
				timeDue = timeUtils.fromFriendlyTime(item[1])
				if timeDue < datetime.now():
					database.deleteManyFromPersistentStore(self.persistentType, item[0])
				else:
					self.away[item[0]] = timeDue

		handler.registerGlobalHandler('away', self)

	def handleAway(self, message):
		if message.numArgs == 1:
			value = self.tryParseTime(message.sender, message.args[0])
			if isinstance(value, datetime):
				self.markTemporaryAway(message.sender, value)
				self.sendMsg(message.sender, constants.getConstant('tempAwayMsg') % timeUtils.getFriendlyTime(value))
				return
			else:
				self.sendMsg(message.sender, value)
				return
		self.setAway(message.sender, True)
		self.sendMsg(message.sender, constants.getConstant('awayMsg'))

	def handleBack(self, message):
		self.setAway(message.sender, False)
		self.sendMsg(message.sender, constants.getConstant('backMsg'))
	
	def setAway(self, user, enabled):
		if user in self.away.keys():
			if not enabled:
				database.deleteManyFromPersistentStore(self.persistentType, user)
				del self.away[user]
			return
		if enabled:
			database.updatePersistentStore(self.persistentType, user, 'None')
			self.away[user] = None

	def markTemporaryAway(self, user, newTime):
		database.updatePersistentStore(self.persistentType, user, timeUtils.getFriendlyTime(newTime))
		self.away[user] = newTime

	def isAway(self, user):
		self.removeUserIfTemporaryAway(user)
		return user in self.away.keys()

	def removeUserIfTemporaryAway(self, user):
		if user in self.away.keys():
			returnTime = self.away[user]
			if returnTime != None:
				if datetime.now() > returnTime:
					database.deleteManyFromPersistentStore(self.persistentType, user)
					del self.away[user]

def __init__(levels):
	AwayHandler(levels)

