import commandHandler
import config
import scheduler
from datetime import datetime, timedelta
from handlerBase import HandlerBase, Command

class ShutdownHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!shutdown', self.handle))

	def handle(self, message):
		if message.numArgs == 4:
			delay = 0
			try:
				delay = int(message.args[1])
			except ValueError:
				self.sendMsg(message.sender, 'usage: !shutdown [-t secs] -h now')
				return

			self.inform(config.channel, 'I am shutting down for maintenance in ' + str(delay) + ' seconds')
			message.args = ['timed shutdown']
			scheduler.addEvent(datetime.now() + timedelta(seconds=delay), [commandHandler.handleQuit, message])
		elif message.numArgs == 2:
			if message.args[0] == '-t':
				self.sendMsg(message.sender, 'usage: !shutdown [-t secs] -h now')
				return
			self.inform(config.channel, 'The system is going down for maintenance NOW')
			message.args = []
			commandHandler.handleQuit(message)
		else:
			self.sendMsg(message.sender, 'usage: !shutdown [-t secs] -h now')

def __init__(levels):
	ShutdownHandler(levels)
