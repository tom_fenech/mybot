import config
import constants
import database
from handlerBase import HandlerBase, Command

class ManiHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels['mani'], Command('!mani', self.handle))
		HandlerBase.__init__(self, levels['maniadd'], Command('!maniadd', self.handleAdd))
		HandlerBase.__init__(self, levels['manidel'], Command('!manidel', self.handleDel))
		
	def handle(self, message):
		if message.numArgs == 1:
			mani = self.getMani(message.args[0])
			self.sendMsg(message.sender, mani)
		else:
			out = [constants.getConstant('maniHelp')]
			out.append(', '.join(self.getManis()))
			self.sendMultilineMsg(message.sender, out)

	def handleAdd(self, message):
		if message.numArgs >= 2:
			if message.args[0].lower() in [x.lower() for x in self.getManis()]:
				self.sendMsg(message.sender, 'The mani \'' + message.args[0] + '\' already exists.')
				return

			self.addMani(message.args[0], ' '.join(message.args[1:]))
			self.sendMsg(message.sender, 'mani \'' + message.args[0] + '\' added')
			self.inform(config.channel, u'New manifesto added. \u0002!mani ' + message.args[0] + u'\u0002 to view')
		else:
			self.sendMsg(message.sender, 'usage: !maniadd <maniName> <description>')

	def handleDel(self, message):
		if message.numArgs == 1:
			self.delMani(message.args[0])
			self.sendMsg(message.sender, 'mani \'' + message.args[0] + '\' deleted')
		else:
			self.sendMsg(message.sender, 'usage: !maniadd <maniName>')

	def getManis(self):
		result = database.execute('select key from mani', '')
		retVal = [x[0] for x in result.fetchall()]
		retVal.sort()
		return retVal

	def getMani(self, name):
		result = database.execute('select value from mani where key LIKE ?', (name,))
		result = result.fetchone()
		if result != None:
			return result[0]
		else:
			return 'The manifesto called \'' + name + '\' was not found'

	def addMani(self, name, description):
		database.execute('insert into mani values (?,?)', (name,description))

	def delMani(self, name):
		database.execute('delete from mani where key like ?', (name,))

def __init__(levels):
	ManiHandler(levels)
