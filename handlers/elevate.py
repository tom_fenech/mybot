from handlerBase import HandlerBase, Command
import handler
import config
import ircCommands
import scheduler
from datetime import datetime, timedelta

class ElevateHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!elevate', self.handle))

	def handle(self, message):
		if message.numArgs == 1:
			channel = message.args[0]
		elif message.numArgs == 0 and message.destination != config.nick:
			channel = message.destination
		else:
			self.sendMsg(message.sender, 'usage: !elevate [channel]')
			return
		ircCommands.op(message.sender, channel, True)
		scheduler.addEvent(datetime.now() + timedelta(minutes=config.elevateDuration), [ircCommands.op, message.sender, channel, False])

def __init__(levels):
	ElevateHandler(levels)
