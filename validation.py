import timeUtils
from datetime import datetime

class ValidationResult:
	def __init__(self, success, value = None, failureReason = None):
		if success is True and value == None:
			raise Exception('You cannot have a successful validation result containing no value')
		if success is False and failureReason == None:
			raise Exception('You cannot have a failing validation result containing no value (used as reason for failure)')

		self.success = success
		self.value = value
		self.failureReason = failureReason

class Validation:
	yes = ['yes', 'y', 'true', '1', 'yep', 'ye', 'yeah', 'yeh', 'aye', 'affirmative', 'indeed']
	fixedOpts = list(yes)
	fixedOpts.append('fixed')
	no = ['no', 'n', 'false', '0', 'nope', 'na', 'nah', 'negative']
	cancelOpts = ['quit', 'cancel']

	@classmethod
	def yesNo(cls, input):
		input = input.lower()
		if input in cls.yes:
			return ValidationResult(True, True)
		if input in cls.no:
			return ValidationResult(True, False)
		else:
			return ValidationResult(False, failureReason='Yes or no?')

	@staticmethod
	def notEmpty(input):
		if len(input.strip()) > 0:
			return ValidationResult(True, input.strip())
		else:
			return ValidationResult(False, failureReason='Enter some text - spaces don\'t count!')

	@classmethod
	def fixed(cls, input):
		input = input.lower()
		if input in cls.fixedOpts:
			return ValidationResult(True, input.strip())
		else:
			return ValidationResult(False, failureReason='Is your internet fixed now?')

	@staticmethod
	def dateTime(input):
		input = input.split(' ', 1)
		if len(input) == 1:
			input.append('')

		dateTime = timeUtils.getDateTime(*input)
		if dateTime.success:
			return ValidationResult(True, dateTime.value)
		else:
			return ValidationResult(False, failureReason=dateTime.error)

	@staticmethod
	def dateTimeAfter(getComparisonValue):
		def _dateTimeAfter(input):
			validationResult = Validation.dateTime(input)
			if validationResult.success:
				comparisonDate = getComparisonValue()
				if validationResult.value > comparisonDate:
					return validationResult
				else:
					return ValidationResult(False, failureReason = 'Sorry, this date must be after ' + timeUtils.getFriendlyTime(comparisonDate))
			return validationResult
		return _dateTimeAfter
			

	@staticmethod
	def date(input):
		date = timeUtils.getDateInFuture(input)
		if not isinstance(date, datetime):
			return ValidationResult(False, failureReason=date)
		return ValidationResult(True, date)

	@staticmethod
	def time(input):
		time = timeUtils.getTimeInFuture(input)
		if not isinstance(time, datetime):
			return ValidationResult(False, failureReason=time)
		return ValidationResult(True, time)

	@classmethod
	def cancel(cls, input):
		if input in cls.cancelOpts:
			return ValidationResult(True, True)
		return ValidationResult(False, failureReason='user did not cancel')

	@staticmethod
	def uniqueEventName(input):
		if input == 'test':
			return ValidationResult(True, input)
		return ValidationResult(False, failureReason='An event called ' + input + ' already exists')

