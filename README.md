mybot is a lightweight python-based IRC bot.

Several different behaviours (handlers) are included in the core repository, and the bot is designed to be easily configurable and extensible by adding additional modules. If you have any ideas (or implementations of new modules) please submit them for inclusion!

Configuration
=============
You will need to create a configuration file to define the behaviour of this IRC bot. This is where you'll set the IRC server to connect to, channels to join, nickname of the bot etc. This is also where you configure the list of available commands and manage user access rights to these commands. See config.example.py for a sample config.

The bot is started on the command line like so:

	./ircbot.py -c /path/to/your.config.py

Once the bot is running, your configuration is accessed through the basic module named `config`. This can be referenced from anywhere needing to access configuration settings, allowing all parts of the system to access config in the same way.

Database
========
The current supported database is SQLite. This was chosen due to its lightweight footprint, ease of deployment, and the fact we expect access to be single-user. The path to the database file must be configured in your config file.

If the bot is started up and the configured database file does not exist, the default behaviour is that start-up will fail. This prevents the bot starting up without an administrator realising its database has gone missing. The flag `--init-missing-database` may be passed on the command line. If present, instead of failing to start, a blank database and its schema will be initialised at the configured location. The bot will then continue to start. This is the recommended way to generate your initial database.

Migrations
==========
The bot uses a simple migration framework to automatically apply any new database changes when the bot starts up. If a developer wants to modify the database, a migration file must be written in the migration directory. You can look at one of the files in the directory for an example. The migration should be dated at the point you are writing it. Migrations are applied at bot start-up, and a record is kept of each migration which has been applied (by date), so it will not be applied again.

Channels and the network
========================
One instance of the bot is capable of connecting to one IRC server. There is a concept of one 'main' channel, used by some handlers and some parts of the core. Generally speaking, though, the bot can join any number of channels on one network, and will provide a consistent experience across those channels.

Handlers
========
Bot functionality is grouped into handlers. Each handler is able to set up any number of triggers (an IRC command the bot will respond to); and these triggers should be related to the purpose of the handler to avoid confusion. Handlers are added or removed in your config file, so this controls the triggers the bot will respond to.

There is an existing library of handlers, some of which are fairly specialised. Some are handlers adding more general behaviour, such as the logging handler (to log all of the channel chat) and the reminder handler (allowing anyone in the channel can set up reminders for themselves).

It is anticipated that most new functionality will be added by implementing a new handler.

Handlers may be reloaded dynamically through the bot command **!reload** (also provided by a handler). This may be useful during development when testing out a handler, or if you need to deploy a change to the handler without restarting the the bot.

Sometimes you may wish to reference other instances of handlers, to make use of their behaviour or modify their state. Handlers may be registered with a global register of handlers, and then retrieved by name.

Users & Permissions
===================
The name of a master admin should be specified in the config file. This user will be able to run any of the triggers, irrespective of the user group required. This allows the master admin user to assign privileges to other users. The master admin will also receive a message (containing stack trace) if anything goes wrong e.g. an unhandled exception. This usually indicates a bug in one of the bot handlers, which you may choose to investigate or report on the issue tracker.

Users may be placed into arbitrary groups, e.g. 'everyone', 'moderators, 'admin'. These groups are then used in the config file to specify which groups are able to access which commands. Handler implementations may choose how granular they want to allow user access to be configured: either one access level configured for all triggers it implements, or allowing the user group to be configured for each trigger.

The bot is able to identify users by checking whether they have identified their registered nickname with nickserv. It follows that users must be registered with nickserv in order to make use of privileged commands. If a user has identified, then the bot will treat them as a member of the groups they are in. If a user has not identified, then they will be treated as a regular user. This applies to the master admin use as well.

Depending upon how your channel is maintained, you may wish to configure automatic 'levels' for some user groups, which will be applied to users when they enter the main channel. For example, you might configure that all moderators will be 'voiced' when they join the channel. As above, users must identify with nickserv for this to work.