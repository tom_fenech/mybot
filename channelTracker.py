import config
import userTracker

channels = { }

def disconnected():
	channels = { }

def join(channel):
	channels[channel] = []

def part(channel):
	if channel in channels:
		del channels[channel]

def botJoined(channel, userList):
	channels[channel] = []

	for user in userList:
		userJoined(channel, user)

def getCurrentChannelList():
	return channels.keys()

def userJoined(channel, user):
	if user[0] in userTracker.opcodes.values():
		user = user[1:]

	channels[channel].append(user)

	if channel in joinPartSubscribers.keys():
		[c(user, 'joined') for c in joinPartSubscribers[channel]]

def userLeft(channel, user):
	channels[channel].remove(user)

	if channel in joinPartSubscribers.keys():
		[c(user, 'left') for c in joinPartSubscribers[channel]]

def userQuit(user):
	for channel, userList in channels.iteritems():
		if user in userList:
			userList.remove(user)

			if channel in joinPartSubscribers.keys():
				[c(user, 'left') for c in joinPartSubscribers[channel]]

def userChangedName(old, new):
	for channel, userList in channels.iteritems():
		if old in userList:
			userList.remove(old)
			userList.append(new)

			if channel in joinPartSubscribers.keys():
				[c(old, 'changedName') for c in joinPartSubscribers[channel]]

def getUserList(channel):
	users = list(channels[channel])
	users.remove(config.nick)
	return users

def getSpamProtectedChannelsUserIsIn(user):
	result = []
	spamPreventionChannels = getattr(config, 'spamPreventionChannels', [])
	for (channelName, channelUserList) in channels.items():
		if channelName in spamPreventionChannels and user in channelUserList:
			result.append(channelName)
	return result

subscribers = { }
def subscribeToMessagesForChannel(channel, callback):
	if channel in subscribers.keys():
		subscribers[channel].append(callback)
	else:
		subscribers[channel] = [callback]

joinPartSubscribers = { }
def subscribeToJoinPartsForChannel(channel, callback):
	if channel in joinPartSubscribers.keys():
		joinPartSubscribers[channel].append(callback)
	else:
		joinPartSubscribers[channel] = [callback]

def messageReceived(message):
	if message.destination in subscribers.keys():
		[c(message) for c in subscribers[message.destination]]
