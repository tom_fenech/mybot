timezoneOverride = 'Europe/London'
database = 'localhost.db'

network = 'localhost'
port = 6667; ssl = False
#bindAddress = '127.0.0.1'

nick = 'MyBot'; channel = '#cogs'; gamingChannel = '#cogs.gaming'; debug = True; testing = False; channelsToJoin = [channel, gamingChannel]
spamPreventionChannels = ['#cogs']
floodPreventionChannels = ['#cogs']

userinfo = 'MyBot myhostname ' + network + ' :MyBot owns'
identString = 'yourNicknamePassword'
masterAdmin = 'chris'

autoAssignLevels = {
	channel: {
		'moderator': 'v'
	}
}

allLevel = 'all'
handlerDirectory = 'handlers'
handlers = {
	'away': allLevel,
	'bash': allLevel,
	'help': allLevel,
	'lan': allLevel,
	'mod': allLevel,
	'remind': allLevel,
	'scratch': allLevel,
	'seen': allLevel,
	'tell': allLevel,
	'userPrefs': allLevel,
	'whois': allLevel,
	'topic': ['npo', 'committee'],
	'elevate': ['committee'],
	'tempban': ['moderator', 'committee'],
	'moderator': 'committee',
	'spam': ['committee', 'moderator'],
	'lists': {'listadd': ['npo', 'committee'], 'gamelistadd': ['npo', 'committee'], 'listdel': 'committee', 'gamelistdel': 'committee', 'adminupdlist': ['npo', 'committee'], 'maintainerupdlist': allLevel},
	'ban': {'ban': 'admin', 'unban': ['moderator', 'committee']},
	'debug': 'admin',
	'inform': 'admin',
	'joinPart': 'admin',
	'kick': 'admin',
	'logger': { 'level': 'admin' },
	'manageLevels': {'voice': 'committee', 'devoice': 'committee', 'op': 'committee', 'deop': 'committee', 'hop': 'admin', 'dehop': 'admin'},
	'me': 'admin',
	'msg': 'admin',
	'quit': 'admin',
	'reload': 'admin',
	'shutdown': 'admin',
	'usergroups' : 'admin',
	'updateConstant': 'admin'
}
